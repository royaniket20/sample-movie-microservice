package com.aniket.vanilajavaapicaller.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CatalogResponse {

    private String userId;
    private List<CatalogItem> catalogItems;

}
