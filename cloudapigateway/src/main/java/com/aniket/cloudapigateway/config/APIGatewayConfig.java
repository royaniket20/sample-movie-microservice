//package com.aniket.cloudapigateway.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.cloud.gateway.route.Route;
//import org.springframework.cloud.gateway.route.RouteLocator;
//import org.springframework.cloud.gateway.route.builder.Buildable;
//import org.springframework.cloud.gateway.route.builder.PredicateSpec;
//import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.function.Function;
//
//@Configuration
//@Slf4j
//public class APIGatewayConfig {
//
//    @Bean
//    public RouteLocator gatewayRouter(RouteLocatorBuilder routeLocatorBuilder) {
//
//        Function<PredicateSpec, Buildable<Route>> predicateSpecBuildableRouterFunctions
//                = item -> item.path("/spring-cloud-config")
//                    .filters(data->data.addRequestHeader("MyHeader","Sample_Header"))
//                    .uri("http://httpbin.org:80");
//        return routeLocatorBuilder
//                .routes()
//                .route(predicateSpecBuildableRouterFunctions)
//                .build();
//    }
//
//
//}
