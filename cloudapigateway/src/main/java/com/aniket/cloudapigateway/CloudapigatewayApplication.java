package com.aniket.cloudapigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudapigatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudapigatewayApplication.class, args);
    }

}
