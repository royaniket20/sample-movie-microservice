package com.aniket.moviecatalogservice.service;

import com.aniket.moviecatalogservice.model.CatalogResponse;

public interface CatalogService {

    public CatalogResponse getCatalogResponse(String userId);
}
