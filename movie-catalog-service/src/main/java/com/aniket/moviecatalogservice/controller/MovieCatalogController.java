package com.aniket.moviecatalogservice.controller;

import com.aniket.moviecatalogservice.model.CatalogItem;
import com.aniket.moviecatalogservice.model.CatalogResponse;
import com.aniket.moviecatalogservice.service.CatalogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/catalog")
@Slf4j
public class MovieCatalogController {

    @Autowired
    private CatalogService catalogService;

//    private List<CatalogItem> catalogItems = new ArrayList<>();
//
//    @PostConstruct
//    public void DataLoad(){
//        for (int i = 0; i < 20; i++) {
//            catalogItems.add(new CatalogItem(i,"Movie_"+i,"Movie Description_"+i , new Double(i)));
//        }
//    }

    @GetMapping("/{userId}")
    public CatalogResponse getItems(@PathVariable("userId") String id){
       log.info("Fetching the Catalog Information for User id : {}",id);

//        catalogItems.stream().forEach(item-> log.info("Fetched Items : {}",item));
//        return  new CatalogResponse(id,catalogItems);

        CatalogResponse catalogResponse = catalogService.getCatalogResponse(id);
        return catalogResponse;
    }
}
