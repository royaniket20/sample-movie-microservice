package com.aniket.movieinfoservice.service.impl;


import com.aniket.movieinfoservice.model.MovieInfo;
import com.aniket.movieinfoservice.service.InfoService;
import com.aniket.movieinfoservice.utils.ServiceCallerUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Random;

@Component
@Slf4j
public class InfoServiceImpl implements InfoService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private  WebClient.Builder webClientBuilderl;

    @Autowired
    private ServiceCallerUtils ServiceCallerUtils;

    @Override
    public MovieInfo getMovieInfoResponse(int movieId) {
        try {
            final int time = new Random().nextInt(1500);
            log.info("Putting system on Sleep for Time : {}",time);
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        MovieInfo movieInfo = ServiceCallerUtils.getMovieInfo(restTemplate, movieId);
        return  movieInfo;
    }
}
