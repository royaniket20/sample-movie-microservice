package com.aniket.movieinfoservice.service;
import com.aniket.movieinfoservice.model.MovieInfo;

public interface InfoService {

    public MovieInfo getMovieInfoResponse(int movieId);
}
