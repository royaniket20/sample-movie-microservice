package com.aniket.movieinfoservice.controller;

import com.aniket.movieinfoservice.model.MovieInfo;
import com.aniket.movieinfoservice.service.InfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/info")
@Slf4j
public class MovieInfoController {
    private final List<MovieInfo> movieInfos = new ArrayList<>();
    @PostConstruct
    public void DataLoad(){
        for (int i = 0; i < 20; i++) {
            movieInfos.add(new MovieInfo(i,"Movie_"+i,"Movie Description_"+i ));
        }
    }


    @Autowired
    private InfoService infoService;

    @GetMapping("/{movieId}")
    public MovieInfo getItems(@PathVariable("movieId") int id){
        log.info("Fetching the Movie Information for  id : {}",id);
        //Calling Static Data
       // final MovieInfo movieInfo = movieInfos.get(id);
        //Calling from External Service
        final MovieInfo movieInfo =infoService.getMovieInfoResponse((id+500));
        log.info("Fetched Items : {}", movieInfo);
        return  movieInfo;
    }
}
