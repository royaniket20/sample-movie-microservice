package com.aniket.movieinfoservice.utils;


import com.aniket.movieinfoservice.model.MovieDBModel;
import com.aniket.movieinfoservice.model.MovieInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@Component
@Slf4j
public class ServiceCallerUtils {

    @Value("${movieDbUrl}")
    private String movieDbUrl;

    @Value("${api_key_movie_db}")
    private String api_key_movie_db;

    @Autowired
    private ServerProperties serverProperties;


    public MovieInfo getMovieInfo(RestTemplate restTemplate, int movieId){
        log.info("external service call :: Calling Rest template for Movie Info from MovieDB");
        final String url = movieDbUrl+movieId+"?api_key="+api_key_movie_db;
        MovieInfo movieInfo = new MovieInfo();
        movieInfo.setId(movieId-500);
        try {
            MovieDBModel movie = restTemplate.getForObject(url, MovieDBModel.class);
            movieInfo.setDescription("SERVER PORT : "+serverProperties.getPort()+"-----"+movie.getOverview());
            movieInfo.setName(movie.getOriginal_title());
        }catch (Exception e){
            log.error("Movie Info Not found for ID : {} --- cause --- {}",movieId,e.getMessage());
        }


        return movieInfo;
    }

    public MovieInfo getMovieInfoViaWebClient(WebClient.Builder builder, int movieId){
        log.info("external service call :: Calling Web Client for Movie Info from MovieDB");
        final String url = movieDbUrl+movieId+"?api_key="+api_key_movie_db;
        MovieInfo movieInfo = new MovieInfo();
        movieInfo.setId(movieId-500);
        try {
        MovieDBModel movie =builder.build().get().uri(url).retrieve().bodyToMono(MovieDBModel.class).block();
        movieInfo.setDescription(movie.getOverview());
        movieInfo.setName(movie.getOriginal_title());
        }catch (Exception e){
            log.error("Movie Info Not found for ID : {}",movieId);
        }

        return movieInfo;
    }
}
