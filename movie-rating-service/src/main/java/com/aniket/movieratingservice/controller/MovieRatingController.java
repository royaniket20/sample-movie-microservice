package com.aniket.movieratingservice.controller;

import com.aniket.movieratingservice.model.MovieRating;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rating")
@Slf4j
public class MovieRatingController {


        private final List<MovieRating> movieRatings = new ArrayList<>();
        @PostConstruct
        public void DataLoad(){
            for (int i = 0; i < 20; i++) {
                movieRatings.add(new MovieRating(i,new Double(i)));
            }
        }

        @GetMapping("/{movieId}")
        public MovieRating getItems(@PathVariable("movieId") int id){
            log.info("Fetching the Movie Rating Information for  id : {}",id);
            final MovieRating movieRating = movieRatings.get(id);
            log.info("Fetched Items : {}", movieRating);
            return  movieRating;
        }
}
